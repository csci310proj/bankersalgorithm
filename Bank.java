
public class Bank {
	int MAX_RESOURCES = 32;

	int[] resources;
	int[][] max_demand;
	int[][] need;

	int[] available;

	int[][] allocation;

	boolean[] finished;

	Bank(int resources, int threads) {
		//Generate resources
		this.resources = new int[resources];
		max_demand = new int[threads][resources];
		need = new int[threads][resources];
		finished = new boolean[threads];
		for (int i = 0; i < resources; i++) {
			this.resources[i] = (int) (Math.random()*MAX_RESOURCES);
		}

		available = new int[resources];
		System.arraycopy(this.resources, 0, available, 0, this.resources.length);

		for (int i = 0; i < finished.length; i++) {
			finished[i] = false;
		}

		allocation = new int[threads][resources];
	}

	public void addThread(int threadId, int[] max) {
		System.arraycopy(max, 0, max_demand[threadId], 0, max.length);
		System.arraycopy(max, 0, need[threadId], 0, max.length);
	}

	public synchronized Boolean request(int threadId, int[] resources_request) {
		System.out.println("Thread "+threadId+" making request "+print1DMatrix(resources_request));

		for (int i = 0; i < available.length; i++) {
			if (resources_request[i] > available[i]) {
				System.out.println("Bank: Thread "+threadId+" must wait");
				return false;
			}
		}

		if (!isRequestSafe(threadId, resources_request)) {
			return false;
		}

		for (int i = 0; i < available.length; i++) {
			available[i] -= resources_request[i];
			need[threadId][i] -= resources_request[i];
			allocation[threadId][i] += resources_request[i];
		}

		return true;
	};

	public synchronized void release(int threadId) {
		System.out.println("Thread "+threadId+" releasing resources:");
		System.out.println(print1DMatrix(allocation[threadId]));

		for (int i = 0; i < allocation[threadId].length; i++) {
			available[i] += allocation[threadId][i];
			need[threadId][i] += allocation[threadId][i];
			allocation[threadId][i] = 0;
		}

		System.out.println("Bank - Available:\n"+print1DMatrix(available));
	};

	private synchronized boolean isRequestSafe(int threadId, int[] resources_request) {
		int[] safe_sequence = new int[need.length]; int ss_idx = 0;
		boolean[] finish = new boolean[need.length];
		int[] work = new int[available.length];
		
		System.arraycopy(available, 0, work, 0, available.length);
		System.arraycopy(finished, 0, finish, 0, finish.length);

		//Update work, allocation, and need to reflect request
		for (int i = 0; i < work.length; i++) {
			work[i] -= resources_request[i];
		}
		for (int i = 0; i < work.length; i++) {
			allocation[threadId][i] += resources_request[i];
		}
		for (int i = 0; i < work.length; i++) {
			need[threadId][i] -= resources_request[i];
		}

		//Safe sequence algorithm
		for (int i = 0; i < finish.length; i++) {
			int j = 0;
			if (finish[i] == false) {
				for (; j < work.length; j++) {
					if (need[i][j] > work[j]) {
						break;
					}
				}
			} else {
				continue;
			}
			if (j == work.length) {
				finish[i]=true;
				for (int k = 0; k < work.length; k++) {
					work[k] += allocation[i][k]; 
				}
				safe_sequence[ss_idx] = i; ss_idx++;
				i = -1; //start over
			}
		}

		//Revert allocation and need arrays
		for (int i = 0; i < work.length; i++) {
			allocation[threadId][i] -= resources_request[i];
		}
		for (int i = 0; i < work.length; i++) {
			need[threadId][i] += resources_request[i];
		}

		for (int i = 0; i < finish.length; i++) {
			if (!finish[i]) {
				System.out.println("Bank - Safe Sequence not found");
				return false;
			}
		}

		System.out.println("Bank - Safe Sequence:\n"+print1DMatrix(safe_sequence));
		
		return true;
	};

	public StringBuffer print1DMatrix(int[] m) {
		StringBuffer s = new StringBuffer();

		s.append("[");
		for (int i = 0; i < m.length; i++) {
			s.append(m[i] + ",");
		}
		s.setCharAt(s.length()-1, ']');

		return s;
	};

	public StringBuffer print2DMatrix(int[][] m) {
		StringBuffer s = new StringBuffer();

		for (int i = 0; i < m.length; i++) {
			s.append(print1DMatrix(m[i]));
			s.append("\n");
		}

		return s;
	}
}
