
public class BAThread extends Thread {
	int threadId;
	Bank bank;
	int requests;
	int[] max_demand;
	static boolean notified = false;

	BAThread (Bank bank, int threadId) {
		this.threadId = threadId;
		this.bank = bank;

		max_demand = new int[bank.resources.length];
		for (int i = 0; i < bank.resources.length; i++) {
			max_demand[i] = (int) (Math.random() * bank.resources[i]);
		}
		bank.addThread(threadId, max_demand);
		requests = (int) (Math.random() * 3) + 3;  // from 3 to 5 requests
	}

	public void run() {
		for (int i = 0; i < requests; i++) {
			int[] request = new int[bank.resources.length];
			for (int j = 0; j < bank.resources.length; j++) {
				request[j] = (int) (Math.random() * max_demand[j]);

				// Make sure the request won't allocate more resources than we have on the machine
				if (request[j] > bank.need[threadId][j]) {
					request[j] = bank.need[threadId][j];
				}
			}

			synchronized(bank) {
				while (!bank.request(threadId, request)) {
					synchronized(bank) {
						try {
							bank.wait();
						} catch (Exception e) {};
					}
				}

				System.out.print("Bank - Allocation matrix:\n"+bank.print2DMatrix(bank.allocation));
				System.out.println("Bank - Available vector:\n"+bank.print1DMatrix(bank.available));
				System.out.println("Thread "+threadId+" request "+i+" granted\n");
			}
			//Let thread hold resources
			try {
				Thread.sleep(((int) (Math.random() * 5) + 1)*1000);
			} catch (Exception e) {};
		}
		synchronized(bank) {
			bank.release(threadId);
			bank.finished[threadId] = true;
			bank.notifyAll();
		}
	}
}
