
public class BankersAlgorithm {
	public static void main(String[] args) {
		int resources = Integer.parseInt(args[0]);
		int threads = Integer.parseInt(args[1]);

		Bank bank = new Bank(resources, threads);

		System.out.println("Bank - Initial Resources Available:");
		System.out.println(bank.print1DMatrix(bank.resources));

		//Generate threads
		Thread[] t = new Thread[threads];
		for (int i = 0; i < threads; i++) {
			t[i] = new Thread(new BAThread(bank, i));
		}
		
		System.out.println("Bank - Max:");
		System.out.println(bank.print2DMatrix(bank.max_demand));

		for (int i = 0; i < threads; i++) {
			t[i].start();
		}

		for (int i = 0; i < threads; i++) {
			try {
				t[i].join();
			} catch (Exception e) {};
		}

		System.out.println("Final Available vector:");
		System.out.println(bank.print1DMatrix(bank.available));

		System.out.println("Final Allocation matrix:");
		System.out.print(bank.print2DMatrix(bank.allocation));

		System.exit(0);
	}
}
